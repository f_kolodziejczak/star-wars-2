import { Component } from '@angular/core';

@Component({
  selector: 'star-wars-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'star-wars';
}
