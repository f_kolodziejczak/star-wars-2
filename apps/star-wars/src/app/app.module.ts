import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StarWarsWebShellModule, StarWarsWebShellRoutingModule } from '@star-wars/star-wars/web/shell';
import { environment } from '../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StarWarsWebSharedCoreModule } from '@star-wars/star-wars/web/shared/core';
import { StarWarsWebPlanetsDataAccessModule } from '@star-wars/star-wars/web/planets/data-access';
import { StarWarsWebSpaceshipsDataAccessModule } from '@star-wars/star-wars/web/spaceships/data-access';
import { StarWarsWebPeopleDataAccessModule } from '@star-wars/star-wars/web/people/data-access';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StarWarsWebShellModule,
    StarWarsWebShellRoutingModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    StarWarsWebSharedCoreModule,
    StarWarsWebPlanetsDataAccessModule,
    StarWarsWebSpaceshipsDataAccessModule,
    StarWarsWebPeopleDataAccessModule,

  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
