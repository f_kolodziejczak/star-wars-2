export * from './lib/+state/people.facade';
export * from './lib/+state/people.reducer';
export * from './lib/+state/people.selectors';
export * from './lib/star-wars-web-people-data-access.module';
