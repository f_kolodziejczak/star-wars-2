import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GetPersonRequestPayload } from '../resources/request-payloads/get-person.request-payload';
import { map } from 'rxjs/operators';
import { PeopleResponse, Person } from '@star-wars/star-wars/web/people/domain';

@Injectable()
export class PeopleDataService {
  readonly endpoints = {
    getPerson: 'https://swapi.dev/api/people/',
    getPersonCollection: 'https://swapi.dev/api/people/',
  };
  constructor(private http: HttpClient) {}

  getPerson(payload: GetPersonRequestPayload): Observable<Person> {
    return this.http.get<Person>(this.endpoints.getPerson + payload.id).pipe(
      map((person) => {
        const mappedPerson = {};
        Object.keys(person).forEach((key) => {
          mappedPerson[PeopleDataService.camelCaseMapper(key)] = person[key];
        });
        return mappedPerson as Person;
      })
    );
  }

  getPeopleCollection(pageNumber: number): Observable<PeopleResponse> {
    return this.http
      .get<PeopleResponse>(
        this.endpoints.getPersonCollection + `?page=${pageNumber}`
      )
      .pipe(
        map((response) => ({
          ...response,
          results: response.results.map((person) => {
            const mappedPerson = {};
            Object.keys(person).forEach((key) => {
              mappedPerson[PeopleDataService.camelCaseMapper(key)] =
                person[key];
            });
            return mappedPerson as Person;
          }),
        }))
      );
  }

  private static camelCaseMapper(variableKey: string): string {
    return variableKey.replace(/([-_][a-z])/gi, (letter) =>
      letter.toUpperCase().replace('_', '')
    );
  }
}
