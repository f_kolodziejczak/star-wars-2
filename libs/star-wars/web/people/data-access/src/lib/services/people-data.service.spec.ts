import { TestBed } from '@angular/core/testing';

import { PeopleDataService } from './people-data.service';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';

describe('PeopleDataService', () => {
  let httpMock: HttpTestingController;

  let service: PeopleDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(PeopleDataService);
  });
  afterEach(() => {
    httpMock.verify();
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getPerson', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.getPerson({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.getPerson);
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.getPerson({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.getPerson);
      expect(req.request.method).toBe('GET');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#getPersonCollection', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.getPersonCollection({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.getPersonCollection);
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.getPersonCollection({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.getPersonCollection);
      expect(req.request.method).toBe('GET');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });
});
