import {
  initialState,
  PEOPLE_FEATURE_KEY,
  PeopleState,
} from './people.reducer';
import { peopleQuery } from './people.selectors';

describe('People Selectors', () => {
  let storeState: { [PEOPLE_FEATURE_KEY]: PeopleState };

  beforeEach(() => {
    storeState = {
      [PEOPLE_FEATURE_KEY]: initialState,
    };
  });

  test('getPerson() returns person value', () => {
    const result = peopleQuery.getPerson(storeState);

    expect(result).toBe(storeState[PEOPLE_FEATURE_KEY].person);
  });

  test('getPersonLoading() returns personLoading value', () => {
    const result = peopleQuery.getPersonLoading(storeState);

    expect(result).toBe(storeState[PEOPLE_FEATURE_KEY].personLoading);
  });

  test('getPersonLoadError() returns personLoadError value', () => {
    const result = peopleQuery.getPersonLoadError(storeState);

    expect(result).toBe(storeState[PEOPLE_FEATURE_KEY].personLoadError);
  });

  test('getPersonCollection() returns personCollection value', () => {
    const result = peopleQuery.getPersonCollection(storeState);

    expect(result).toBe(storeState[PEOPLE_FEATURE_KEY].personCollection);
  });

  test('getPersonCollectionLoading() returns personCollectionLoading value', () => {
    const result = peopleQuery.getPersonCollectionLoading(storeState);

    expect(result).toBe(storeState[PEOPLE_FEATURE_KEY].personCollectionLoading);
  });

  test('getPersonCollectionLoadError() returns personCollectionLoadError value', () => {
    const result = peopleQuery.getPersonCollectionLoadError(storeState);

    expect(result).toBe(
      storeState[PEOPLE_FEATURE_KEY].personCollectionLoadError
    );
  });
});
