import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PEOPLE_FEATURE_KEY, PeopleState } from './people.reducer';

// Lookup the 'People' feature state managed by NgRx
const getPeopleState = createFeatureSelector<PeopleState>(PEOPLE_FEATURE_KEY);

const getPerson = createSelector(getPeopleState, (state) => state.person);

const getPersonLoading = createSelector(
  getPeopleState,
  (state) => state.personLoading
);

const getPersonLoadError = createSelector(
  getPeopleState,
  (state) => state.personLoadError
);

const getPersonCollection = createSelector(
  getPeopleState,
  (state) => state.personCollection
);

const getPersonCollectionLoading = createSelector(
  getPeopleState,
  (state) => state.personCollectionLoading
);

const getPersonCollectionLoadError = createSelector(
  getPeopleState,
  (state) => state.personCollectionLoadError
);

export const peopleQuery = {
  getPerson,
  getPersonLoading,
  getPersonLoadError,
  getPersonCollection,
  getPersonCollectionLoading,
  getPersonCollectionLoadError,
};
