import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PeoplePartialState } from './people.reducer';
import { peopleQuery } from './people.selectors';
import { fromPeopleActions } from './people.actions';
import { GetPersonRequestPayload } from '../resources/request-payloads/get-person.request-payload';
import { GetPersonCollectionRequestPayload } from '../resources/request-payloads/get-person-collection.request-payload';

@Injectable()
export class PeopleFacade {
  person$ = this.store.pipe(select(peopleQuery.getPerson));
  personLoading$ = this.store.pipe(select(peopleQuery.getPersonLoading));
  personLoadError$ = this.store.pipe(select(peopleQuery.getPersonLoadError));
  personCollection$ = this.store.pipe(select(peopleQuery.getPersonCollection));
  personCollectionLoading$ = this.store.pipe(
    select(peopleQuery.getPersonCollectionLoading)
  );
  personCollectionLoadError$ = this.store.pipe(
    select(peopleQuery.getPersonCollectionLoadError)
  );
  constructor(private store: Store<PeoplePartialState>) {}

  getPerson(data: GetPersonRequestPayload): void {
    this.store.dispatch(new fromPeopleActions.GetPerson(data));
  }

  getPersonCollection(data: GetPersonCollectionRequestPayload): void {
    this.store.dispatch(new fromPeopleActions.GetPersonCollection(data));
  }
}
