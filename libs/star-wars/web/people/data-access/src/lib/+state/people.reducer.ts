import { fromPeopleActions } from './people.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { PeopleResponse, Person } from '@star-wars/star-wars/web/people/domain';

export const PEOPLE_FEATURE_KEY = 'people';

export interface PeopleState {
  person: Person | null;
  personLoading: boolean;
  personLoadError: HttpErrorResponse | null;
  personCollection: PeopleResponse | null;
  personCollectionLoading: boolean;
  personCollectionLoadError: HttpErrorResponse | null;
}

export interface PeoplePartialState {
  readonly [PEOPLE_FEATURE_KEY]: PeopleState;
}

export const initialState: PeopleState = {
  person: null,
  personLoading: false,
  personLoadError: null,
  personCollection: null,
  personCollectionLoading: false,
  personCollectionLoadError: null,
};

export function peopleReducer(
  state: PeopleState = initialState,
  action: fromPeopleActions.CollectiveType
): PeopleState {
  switch (action.type) {
    case fromPeopleActions.Types.GetPerson: {
      state = {
        ...state,
        person: null,
        personLoading: true,
        personLoadError: null,
      };
      break;
    }

    case fromPeopleActions.Types.GetPersonFail: {
      state = {
        ...state,
        person: null,
        personLoading: false,
        personLoadError: action.payload,
      };
      break;
    }

    case fromPeopleActions.Types.GetPersonSuccess: {
      state = {
        ...state,
        person: action.payload,
        personLoading: false,
        personLoadError: null,
      };
      break;
    }

    case fromPeopleActions.Types.GetPersonCollection: {
      state = {
        ...state,
        personCollection: null,
        personCollectionLoading: true,
        personCollectionLoadError: null,
      };
      break;
    }

    case fromPeopleActions.Types.GetPersonCollectionFail: {
      state = {
        ...state,
        personCollection: null,
        personCollectionLoading: false,
        personCollectionLoadError: action.payload,
      };
      break;
    }

    case fromPeopleActions.Types.GetPersonCollectionSuccess: {
      state = {
        ...state,
        personCollection: action.payload,
        personCollectionLoading: false,
        personCollectionLoadError: null,
      };
      break;
    }
  }

  return state;
}
