import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { PeopleFacade } from './people.facade';
import { fromPeopleActions } from './people.actions';

describe('PeopleFacade', () => {
  let actions: Observable<any>;
  let facade: PeopleFacade;
  let store: MockStore;

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [],
        providers: [
          PeopleFacade,
          provideMockStore(),
          provideMockActions(() => actions),
        ],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [NxModule.forRoot(), CustomFeatureModule],
      })
      class RootModule {}

      TestBed.configureTestingModule({ imports: [RootModule] });
      facade = TestBed.inject(PeopleFacade);
      store = TestBed.inject(MockStore);
      jest.spyOn(store, 'dispatch');
    });

    describe('#getPerson', () => {
      test('should dispatch fromPeopleActions.GetPerson action', () => {
        const payload = {} as any;
        const action = new fromPeopleActions.GetPerson(payload);

        facade.getPerson(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#getPersonCollection', () => {
      test('should dispatch fromPeopleActions.GetPersonCollection action', () => {
        const payload = {} as any;
        const action = new fromPeopleActions.GetPersonCollection(payload);

        facade.getPersonCollection(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });
  });
});
