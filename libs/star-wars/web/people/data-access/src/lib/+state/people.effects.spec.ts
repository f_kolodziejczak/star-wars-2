import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule, DataPersistence } from '@nrwl/angular';
import { cold, hot } from 'jest-marbles';
import { PeopleEffects } from './people.effects';
import { fromPeopleActions } from './people.actions';
import { PeopleDataService } from '../services/people-data.service';
import { createSpyObj } from 'jest-createspyobj';

describe('PeopleEffects', () => {
  let peopleDataService: jest.Mocked<PeopleDataService>;
  let actions: Observable<any>;
  let effects: PeopleEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        PeopleEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore({ initialState: {} }),
        {
          provide: PeopleDataService,
          useValue: createSpyObj(PeopleDataService),
        },
      ],
    });

    effects = TestBed.inject(PeopleEffects);
    peopleDataService = TestBed.inject(
      PeopleDataService
    ) as jest.Mocked<PeopleDataService>;
  });

  describe('getPerson$', () => {
    test('returns GetPersonSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPerson({} as any);
      const completion = new fromPeopleActions.GetPersonSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      peopleDataService.getPerson.mockReturnValue(response);

      expect(effects.getPerson$).toSatisfyOnFlush(() => {
        expect(peopleDataService.getPerson).toHaveBeenCalled();
      });
      expect(effects.getPerson$).toBeObservable(expected);
    });

    test('returns GetPersonFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPerson({} as any);
      const completion = new fromPeopleActions.GetPersonFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      peopleDataService.getPerson.mockReturnValue(response);

      expect(effects.getPerson$).toSatisfyOnFlush(() => {
        expect(peopleDataService.getPerson).toHaveBeenCalled();
      });
      expect(effects.getPerson$).toBeObservable(expected);
    });
  });

  describe('getPersonCollection$', () => {
    test('returns GetPersonCollectionSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonCollection({} as any);
      const completion = new fromPeopleActions.GetPersonCollectionSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      peopleDataService.getPersonCollection.mockReturnValue(response);

      expect(effects.getPersonCollection$).toSatisfyOnFlush(() => {
        expect(peopleDataService.getPersonCollection).toHaveBeenCalled();
      });
      expect(effects.getPersonCollection$).toBeObservable(expected);
    });

    test('returns GetPersonCollectionFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonCollection({} as any);
      const completion = new fromPeopleActions.GetPersonCollectionFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      peopleDataService.getPersonCollection.mockReturnValue(response);

      expect(effects.getPersonCollection$).toSatisfyOnFlush(() => {
        expect(peopleDataService.getPersonCollection).toHaveBeenCalled();
      });
      expect(effects.getPersonCollection$).toBeObservable(expected);
    });
  });
});
