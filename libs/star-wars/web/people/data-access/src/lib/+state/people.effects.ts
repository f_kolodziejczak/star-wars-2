import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fromPeopleActions } from './people.actions';
import { PeopleDataService } from '../services/people-data.service';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class PeopleEffects {
  getPerson$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPeopleActions.Types.GetPerson),
      mergeMap((action: fromPeopleActions.GetPerson) =>
        this.peopleDataService.getPerson(action.payload)
      ),
      map((data) => new fromPeopleActions.GetPersonSuccess(data)),
      catchError((error) => of(new fromPeopleActions.GetPersonFail(error)))
    )
  );

  getPeople = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPeopleActions.Types.GetPersonCollection),
      mergeMap((action: fromPeopleActions.GetPersonCollection) =>
        this.peopleDataService.getPeopleCollection(action.payload.page)
      ),
      map((data) => new fromPeopleActions.GetPersonCollectionSuccess(data)),
      catchError((error) =>
        of(new fromPeopleActions.GetPersonCollectionFail(error))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private peopleDataService: PeopleDataService
  ) {}
}
