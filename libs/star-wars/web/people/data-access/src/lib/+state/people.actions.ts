import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { GetPersonRequestPayload } from '../resources/request-payloads/get-person.request-payload';
import { GetPersonCollectionRequestPayload } from '../resources/request-payloads/get-person-collection.request-payload';
import { PeopleResponse, Person } from '@star-wars/star-wars/web/people/domain';

export namespace fromPeopleActions {
  export enum Types {
    GetPerson = '[People] Get Person',
    GetPersonFail = '[People] Get Person Fail',
    GetPersonSuccess = '[People] Get Person Success',
    GetPersonCollection = '[People] Get Person Collection',
    GetPersonCollectionFail = '[People] Get Person Collection Fail',
    GetPersonCollectionSuccess = '[People] Get Person Collection Success',
  }

  export class GetPerson implements Action {
    readonly type = Types.GetPerson;

    constructor(public payload: GetPersonRequestPayload) {}
  }

  export class GetPersonFail implements Action {
    readonly type = Types.GetPersonFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetPersonSuccess implements Action {
    readonly type = Types.GetPersonSuccess;

    constructor(public payload: Person) {}
  }

  export class GetPersonCollection implements Action {
    readonly type = Types.GetPersonCollection;

    constructor(public payload: GetPersonCollectionRequestPayload) {}
  }

  export class GetPersonCollectionFail implements Action {
    readonly type = Types.GetPersonCollectionFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetPersonCollectionSuccess implements Action {
    readonly type = Types.GetPersonCollectionSuccess;

    constructor(public payload: PeopleResponse) {}
  }

  export type CollectiveType =
    | GetPerson
    | GetPersonFail
    | GetPersonSuccess
    | GetPersonCollection
    | GetPersonCollectionFail
    | GetPersonCollectionSuccess;
}
