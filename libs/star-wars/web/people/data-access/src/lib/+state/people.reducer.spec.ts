import { fromPeopleActions } from './people.actions';
import { PeopleState, initialState, peopleReducer } from './people.reducer';
import { statesEqual } from '@valueadd/testing';

describe('People Reducer', () => {
  let state: PeopleState;

  beforeEach(() => {
    state = { ...initialState };
  });

  describe('unknown action', () => {
    test('returns the initial state', () => {
      const action = {} as any;
      const result = peopleReducer(state, action);

      expect(result).toBe(state);
    });
  });

  describe('GetPerson', () => {
    test('sets person, personLoading, personLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPerson(payload);
      const result = peopleReducer(state, action);

      expect(result.person).toEqual(null);
      expect(result.personLoading).toEqual(true);
      expect(result.personLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'person',
          'personLoading',
          'personLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPersonFail', () => {
    test('sets person, personLoading, personLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonFail(payload);
      const result = peopleReducer(state, action);

      expect(result.person).toEqual(null);
      expect(result.personLoading).toEqual(false);
      expect(result.personLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'person',
          'personLoading',
          'personLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPersonSuccess', () => {
    test('sets person, personLoading, personLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonSuccess(payload);
      const result = peopleReducer(state, action);

      expect(result.person).toEqual(payload);
      expect(result.personLoading).toEqual(false);
      expect(result.personLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'person',
          'personLoading',
          'personLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPersonCollection', () => {
    test('sets personCollection, personCollectionLoading, personCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonCollection(payload);
      const result = peopleReducer(state, action);

      expect(result.personCollection).toEqual([]);
      expect(result.personCollectionLoading).toEqual(true);
      expect(result.personCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'personCollection',
          'personCollectionLoading',
          'personCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPersonCollectionFail', () => {
    test('sets personCollection, personCollectionLoading, personCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonCollectionFail(payload);
      const result = peopleReducer(state, action);

      expect(result.personCollection).toEqual([]);
      expect(result.personCollectionLoading).toEqual(false);
      expect(result.personCollectionLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'personCollection',
          'personCollectionLoading',
          'personCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPersonCollectionSuccess', () => {
    test('sets personCollection, personCollectionLoading, personCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPeopleActions.GetPersonCollectionSuccess(payload);
      const result = peopleReducer(state, action);

      expect(result.personCollection).toEqual(payload);
      expect(result.personCollectionLoading).toEqual(false);
      expect(result.personCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'personCollection',
          'personCollectionLoading',
          'personCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });
});
