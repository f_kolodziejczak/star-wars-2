import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  PEOPLE_FEATURE_KEY,
  initialState as peopleInitialState,
  peopleReducer,
} from './+state/people.reducer';
import { PeopleEffects } from './+state/people.effects';
import { PeopleFacade } from './+state/people.facade';
import { PeopleDataService } from './services/people-data.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(PEOPLE_FEATURE_KEY, peopleReducer, {
      initialState: peopleInitialState,
    }),
    EffectsModule.forFeature([PeopleEffects]),
  ],
  providers: [PeopleFacade, PeopleDataService],
})
export class StarWarsWebPeopleDataAccessModule {}
