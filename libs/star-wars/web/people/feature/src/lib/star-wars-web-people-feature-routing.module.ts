import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { PeopleComponent } from './people/people.component';

const routes: Route[] = [
  {
    path: '',
    component: PeopleComponent
  }
]


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StarWarsWebPeopleFeatureRoutingModule { }
