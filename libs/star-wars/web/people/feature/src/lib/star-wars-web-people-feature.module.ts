import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleComponent } from './people/people.component';
import { StarWarsWebSharedUiTableModule } from '@star-wars/star-wars/web/shared/ui-table';
import { PeopleFacade } from '@star-wars/star-wars/web/people/data-access';
import { MatDialogModule } from '@angular/material/dialog';
import { StarWarsWebPeopleFeatureRoutingModule } from './star-wars-web-people-feature-routing.module';

@NgModule({
  imports: [
    CommonModule,
    StarWarsWebPeopleFeatureRoutingModule,
    StarWarsWebSharedUiTableModule,
    MatDialogModule,
  ],
  declarations: [PeopleComponent],
  providers: [PeopleFacade]
})
export class StarWarsWebPeopleFeatureModule {}
