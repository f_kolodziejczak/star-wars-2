import { Component, OnDestroy, OnInit } from '@angular/core';
import { PeopleFacade } from '@star-wars/star-wars/web/people/data-access';
import { Column } from '@star-wars/star-wars/web/domain';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { DetailsDialogComponent } from '@star-wars/star-wars/web/shared/ui-details-dialog';
import { PeopleResponse, Person } from '@star-wars/star-wars/web/people/domain';

@Component({
  selector: 'star-wars-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss'],
})
export class PeopleComponent implements OnInit, OnDestroy {
  people$ = this.peopleFacade.personCollection$;
  peopleLoading$ = this.peopleFacade.personCollectionLoading$;
  peopleData: PeopleResponse;
  columns: Column[] = [
    { columnId: 'name', displayedName: 'Name' },
    { columnId: 'birthYear', displayedName: 'Birth Year' },
    { columnId: 'eyeColor', displayedName: 'Eye Color' },
    { columnId: 'gender', displayedName: 'Gender' },
    { columnId: 'hairColor', displayedName: 'Hair Color' },
    { columnId: 'height', displayedName: 'Height' },
    { columnId: 'mass', displayedName: 'Mass' },
    { columnId: 'skinColor', displayedName: 'Skin Color' },
    { columnId: 'homeworld', displayedName: 'Homeworld' },
  ];
  private componentDestroyed$ = new Subject();
  constructor(private peopleFacade: PeopleFacade, private dialog: MatDialog) {
    this.peopleFacade.getPersonCollection({ page: 1 });
  }

  ngOnInit(): void {
    this.getInitialData();
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  openDetailsDialog(person: Person): void {
    this.peopleFacade.getPerson({
      id: person.url.split('/').slice(-2)[0] + '/',
    });
    this.dialog.open(DetailsDialogComponent, {
      width: '500px',
      maxHeight: '800px',
      data: { title: person.name, data$: this.peopleFacade.person$ },
    });
  }

  loadPaginatedData(page: PageEvent): void {
    if (
      (page.pageIndex > page.previousPageIndex && this.peopleData.next) ||
      (page.pageIndex < page.previousPageIndex && this.peopleData.previous)
    ) {
      this.peopleFacade.getPersonCollection({ page: page.pageIndex + 1 });
    }
  }

  private getInitialData(): void {
    this.people$.pipe(takeUntil(this.componentDestroyed$)).subscribe((data) => {
      if (data) {
        this.peopleData = {
          results: data.results,
          count: data.count,
          next: data.next,
          previous: data.previous,
        };
      }
    });
  }
}
