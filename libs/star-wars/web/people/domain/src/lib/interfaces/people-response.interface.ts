import { Person } from './person.interface';
import { TableData } from '@star-wars/star-wars/web/domain';

export interface PeopleResponse extends TableData {
  results: Person[];
}
