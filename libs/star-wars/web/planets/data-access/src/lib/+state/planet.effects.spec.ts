import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule, DataPersistence } from '@nrwl/angular';
import { cold, hot } from 'jest-marbles';
import { PlanetEffects } from './planet.effects';
import { fromPlanetActions } from './planet.actions';
import { PlanetDataService } from '../services/planet-data.service';
import { createSpyObj } from 'jest-createspyobj';

describe('PlanetEffects', () => {
  let planetDataService: jest.Mocked<PlanetDataService>;
  let actions: Observable<any>;
  let effects: PlanetEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        PlanetEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore({ initialState: {} }),
        {
          provide: PlanetDataService,
          useValue: createSpyObj(PlanetDataService),
        },
      ],
    });

    effects = TestBed.inject(PlanetEffects);
    planetDataService = TestBed.inject(
      PlanetDataService
    ) as jest.Mocked<PlanetDataService>;
  });

  describe('getPlanet$', () => {
    test('returns GetPlanetSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanet({} as any);
      const completion = new fromPlanetActions.GetPlanetSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      planetDataService.getPlanet.mockReturnValue(response);

      expect(effects.getPlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.getPlanet).toHaveBeenCalled();
      });
      expect(effects.getPlanet$).toBeObservable(expected);
    });

    test('returns GetPlanetFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanet({} as any);
      const completion = new fromPlanetActions.GetPlanetFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      planetDataService.getPlanet.mockReturnValue(response);

      expect(effects.getPlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.getPlanet).toHaveBeenCalled();
      });
      expect(effects.getPlanet$).toBeObservable(expected);
    });
  });

  describe('getPlanetCollection$', () => {
    test('returns GetPlanetCollectionSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetCollection({} as any);
      const completion = new fromPlanetActions.GetPlanetCollectionSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      planetDataService.getPlanetCollection.mockReturnValue(response);

      expect(effects.getPlanetCollection$).toSatisfyOnFlush(() => {
        expect(planetDataService.getPlanetCollection).toHaveBeenCalled();
      });
      expect(effects.getPlanetCollection$).toBeObservable(expected);
    });

    test('returns GetPlanetCollectionFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetCollection({} as any);
      const completion = new fromPlanetActions.GetPlanetCollectionFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      planetDataService.getPlanetCollection.mockReturnValue(response);

      expect(effects.getPlanetCollection$).toSatisfyOnFlush(() => {
        expect(planetDataService.getPlanetCollection).toHaveBeenCalled();
      });
      expect(effects.getPlanetCollection$).toBeObservable(expected);
    });
  });

  describe('createPlanet$', () => {
    test('returns CreatePlanetSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.CreatePlanet({} as any);
      const completion = new fromPlanetActions.CreatePlanetSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      planetDataService.createPlanet.mockReturnValue(response);

      expect(effects.createPlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.createPlanet).toHaveBeenCalled();
      });
      expect(effects.createPlanet$).toBeObservable(expected);
    });

    test('returns CreatePlanetFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.CreatePlanet({} as any);
      const completion = new fromPlanetActions.CreatePlanetFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      planetDataService.createPlanet.mockReturnValue(response);

      expect(effects.createPlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.createPlanet).toHaveBeenCalled();
      });
      expect(effects.createPlanet$).toBeObservable(expected);
    });
  });

  describe('updatePlanet$', () => {
    test('returns UpdatePlanetSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.UpdatePlanet({} as any);
      const completion = new fromPlanetActions.UpdatePlanetSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      planetDataService.updatePlanet.mockReturnValue(response);

      expect(effects.updatePlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.updatePlanet).toHaveBeenCalled();
      });
      expect(effects.updatePlanet$).toBeObservable(expected);
    });

    test('returns UpdatePlanetFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.UpdatePlanet({} as any);
      const completion = new fromPlanetActions.UpdatePlanetFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      planetDataService.updatePlanet.mockReturnValue(response);

      expect(effects.updatePlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.updatePlanet).toHaveBeenCalled();
      });
      expect(effects.updatePlanet$).toBeObservable(expected);
    });
  });

  describe('removePlanet$', () => {
    test('returns RemovePlanetSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.RemovePlanet({} as any);
      const completion = new fromPlanetActions.RemovePlanetSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      planetDataService.removePlanet.mockReturnValue(response);

      expect(effects.removePlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.removePlanet).toHaveBeenCalled();
      });
      expect(effects.removePlanet$).toBeObservable(expected);
    });

    test('returns RemovePlanetFail action on fail', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.RemovePlanet({} as any);
      const completion = new fromPlanetActions.RemovePlanetFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      planetDataService.removePlanet.mockReturnValue(response);

      expect(effects.removePlanet$).toSatisfyOnFlush(() => {
        expect(planetDataService.removePlanet).toHaveBeenCalled();
      });
      expect(effects.removePlanet$).toBeObservable(expected);
    });
  });
});
