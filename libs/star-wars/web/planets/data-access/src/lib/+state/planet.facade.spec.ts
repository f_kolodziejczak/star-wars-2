import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { PlanetFacade } from './planet.facade';
import { fromPlanetActions } from './planet.actions';

describe('PlanetFacade', () => {
  let actions: Observable<any>;
  let facade: PlanetFacade;
  let store: MockStore;

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [],
        providers: [
          PlanetFacade,
          provideMockStore(),
          provideMockActions(() => actions),
        ],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [NxModule.forRoot(), CustomFeatureModule],
      })
      class RootModule {}

      TestBed.configureTestingModule({ imports: [RootModule] });
      facade = TestBed.inject(PlanetFacade);
      store = TestBed.inject(MockStore);
      jest.spyOn(store, 'dispatch');
    });

    describe('#getPlanet', () => {
      test('should dispatch fromPlanetActions.GetPlanet action', () => {
        const payload = {} as any;
        const action = new fromPlanetActions.GetPlanet(payload);

        facade.getPlanet(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#getPlanetCollection', () => {
      test('should dispatch fromPlanetActions.GetPlanetCollection action', () => {
        const payload = {} as any;
        const action = new fromPlanetActions.GetPlanetCollection(payload);

        facade.getPlanetCollection(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#createPlanet', () => {
      test('should dispatch fromPlanetActions.CreatePlanet action', () => {
        const payload = {} as any;
        const action = new fromPlanetActions.CreatePlanet(payload);

        facade.createPlanet(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#updatePlanet', () => {
      test('should dispatch fromPlanetActions.UpdatePlanet action', () => {
        const payload = {} as any;
        const action = new fromPlanetActions.UpdatePlanet(payload);

        facade.updatePlanet(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#removePlanet', () => {
      test('should dispatch fromPlanetActions.RemovePlanet action', () => {
        const payload = {} as any;
        const action = new fromPlanetActions.RemovePlanet(payload);

        facade.removePlanet(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });
  });
});
