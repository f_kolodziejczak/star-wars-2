import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PlanetPartialState } from './planet.reducer';
import { planetQuery } from './planet.selectors';
import { fromPlanetActions } from './planet.actions';
import { GetPlanetRequestPayload } from '../resources/request-payloads/get-planet.request-payload';

@Injectable()
export class PlanetFacade {
  planet$ = this.store.pipe(select(planetQuery.getPlanet));
  planetLoading$ = this.store.pipe(select(planetQuery.getPlanetLoading));
  planetLoadError$ = this.store.pipe(select(planetQuery.getPlanetLoadError));
  planetCollection$ = this.store.pipe(select(planetQuery.getPlanetCollection));
  planetCollectionLoading$ = this.store.pipe(
    select(planetQuery.getPlanetCollectionLoading)
  );
  planetCollectionLoadError$ = this.store.pipe(
    select(planetQuery.getPlanetCollectionLoadError)
  );
  constructor(private store: Store<PlanetPartialState>) {}

  getPlanet(data: GetPlanetRequestPayload): void {
    this.store.dispatch(new fromPlanetActions.GetPlanet(data));
  }

  getPlanetCollection(page: number): void {
    this.store.dispatch(new fromPlanetActions.GetPlanetCollection(page));
  }
}
