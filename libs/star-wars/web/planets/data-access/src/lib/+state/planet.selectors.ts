import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PLANET_FEATURE_KEY, PlanetState } from './planet.reducer';

// Lookup the 'Planet' feature state managed by NgRx
const getPlanetState = createFeatureSelector<PlanetState>(PLANET_FEATURE_KEY);

const getPlanet = createSelector(getPlanetState, (state) => state.planet);

const getPlanetLoading = createSelector(
  getPlanetState,
  (state) => state.planetLoading
);

const getPlanetLoadError = createSelector(
  getPlanetState,
  (state) => state.planetLoadError
);

const getPlanetCollection = createSelector(
  getPlanetState,
  (state) => state.planetCollection
);

const getPlanetCollectionLoading = createSelector(
  getPlanetState,
  (state) => state.planetCollectionLoading
);

const getPlanetCollectionLoadError = createSelector(
  getPlanetState,
  (state) => state.planetCollectionLoadError
);

export const planetQuery = {
  getPlanet,
  getPlanetLoading,
  getPlanetLoadError,
  getPlanetCollection,
  getPlanetCollectionLoading,
  getPlanetCollectionLoadError,
};
