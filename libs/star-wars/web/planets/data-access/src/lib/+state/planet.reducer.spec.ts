import { fromPlanetActions } from './planet.actions';
import { PlanetState, initialState, planetReducer } from './planet.reducer';
import { statesEqual } from '@valueadd/testing';

describe('Planet Reducer', () => {
  let state: PlanetState;

  beforeEach(() => {
    state = { ...initialState };
  });

  describe('unknown action', () => {
    test('returns the initial state', () => {
      const action = {} as any;
      const result = planetReducer(state, action);

      expect(result).toBe(state);
    });
  });

  describe('GetPlanet', () => {
    test('sets planet, planetLoading, planetLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanet(payload);
      const result = planetReducer(state, action);

      expect(result.planet).toEqual(null);
      expect(result.planetLoading).toEqual(true);
      expect(result.planetLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planet',
          'planetLoading',
          'planetLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPlanetFail', () => {
    test('sets planet, planetLoading, planetLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetFail(payload);
      const result = planetReducer(state, action);

      expect(result.planet).toEqual(null);
      expect(result.planetLoading).toEqual(false);
      expect(result.planetLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'planet',
          'planetLoading',
          'planetLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPlanetSuccess', () => {
    test('sets planet, planetLoading, planetLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetSuccess(payload);
      const result = planetReducer(state, action);

      expect(result.planet).toEqual(payload);
      expect(result.planetLoading).toEqual(false);
      expect(result.planetLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planet',
          'planetLoading',
          'planetLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPlanetCollection', () => {
    test('sets planetCollection, planetCollectionLoading, planetCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetCollection(payload);
      const result = planetReducer(state, action);

      expect(result.planetCollection).toEqual([]);
      expect(result.planetCollectionLoading).toEqual(true);
      expect(result.planetCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planetCollection',
          'planetCollectionLoading',
          'planetCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPlanetCollectionFail', () => {
    test('sets planetCollection, planetCollectionLoading, planetCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetCollectionFail(payload);
      const result = planetReducer(state, action);

      expect(result.planetCollection).toEqual([]);
      expect(result.planetCollectionLoading).toEqual(false);
      expect(result.planetCollectionLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'planetCollection',
          'planetCollectionLoading',
          'planetCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetPlanetCollectionSuccess', () => {
    test('sets planetCollection, planetCollectionLoading, planetCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.GetPlanetCollectionSuccess(payload);
      const result = planetReducer(state, action);

      expect(result.planetCollection).toEqual(payload);
      expect(result.planetCollectionLoading).toEqual(false);
      expect(result.planetCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planetCollection',
          'planetCollectionLoading',
          'planetCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('CreatePlanet', () => {
    test('sets planetCreating, planetCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.CreatePlanet(payload);
      const result = planetReducer(state, action);

      expect(result.planetCreating).toEqual(true);
      expect(result.planetCreateError).toEqual(null);
      expect(
        statesEqual(result, state, ['planetCreating', 'planetCreateError'])
      ).toBeTruthy();
    });
  });

  describe('CreatePlanetFail', () => {
    test('sets planetCreating, planetCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.CreatePlanetFail(payload);
      const result = planetReducer(state, action);

      expect(result.planetCreating).toEqual(false);
      expect(result.planetCreateError).toEqual(payload);
      expect(
        statesEqual(result, state, ['planetCreating', 'planetCreateError'])
      ).toBeTruthy();
    });
  });

  describe('CreatePlanetSuccess', () => {
    test('sets planetCollection, planetCreating, planetCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.CreatePlanetSuccess(payload);
      const result = planetReducer(state, action);

      expect(result.planetCollection.length).toEqual(1);
      expect(result.planetCreating).toEqual(false);
      expect(result.planetCreateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planetCollection',
          'planetCreating',
          'planetCreateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('UpdatePlanet', () => {
    test('sets planetUpdating, planetUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.UpdatePlanet(payload);
      const result = planetReducer(state, action);

      expect(result.planetUpdating).toEqual(true);
      expect(result.planetUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, ['planetUpdating', 'planetUpdateError'])
      ).toBeTruthy();
    });
  });

  describe('UpdatePlanetFail', () => {
    test('sets planetUpdating, planetUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.UpdatePlanetFail(payload);
      const result = planetReducer(state, action);

      expect(result.planetUpdating).toEqual(false);
      expect(result.planetUpdateError).toEqual(payload);
      expect(
        statesEqual(result, state, ['planetUpdating', 'planetUpdateError'])
      ).toBeTruthy();
    });
  });

  describe('UpdatePlanetSuccess', () => {
    test('sets planetCollection, planetUpdating, planetUpdateError and does not modify other state properties', () => {
      state = {
        ...initialState,
        planetCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromPlanetActions.UpdatePlanetSuccess(payload);
      const result = planetReducer(state, action);

      expect((result as any).planetCollection[0].name).toEqual('test2');
      expect((result as any).planetUpdating).toEqual(false);
      expect((result as any).planetUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planetCollection',
          'planetUpdating',
          'planetUpdateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('RemovePlanet', () => {
    test('sets planetRemoving, planetRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.RemovePlanet(payload);
      const result = planetReducer(state, action);

      expect(result.planetRemoving).toEqual(true);
      expect(result.planetRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, ['planetRemoving', 'planetRemoveError'])
      ).toBeTruthy();
    });
  });

  describe('RemovePlanetFail', () => {
    test('sets planetRemoving, planetRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromPlanetActions.RemovePlanetFail(payload);
      const result = planetReducer(state, action);

      expect(result.planetRemoving).toEqual(false);
      expect(result.planetRemoveError).toEqual(payload);
      expect(
        statesEqual(result, state, ['planetRemoving', 'planetRemoveError'])
      ).toBeTruthy();
    });
  });

  describe('RemovePlanetSuccess', () => {
    test('sets planetCollection, planetRemoving, planetRemoveError and does not modify other state properties', () => {
      state = {
        ...initialState,
        planetCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromPlanetActions.RemovePlanetSuccess(payload);
      const result = planetReducer(state, action);

      expect(result.planetCollection.length).toEqual(0);
      expect(result.planetRemoving).toEqual(false);
      expect(result.planetRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'planetCollection',
          'planetRemoving',
          'planetRemoveError',
        ])
      ).toBeTruthy();
    });
  });
});
