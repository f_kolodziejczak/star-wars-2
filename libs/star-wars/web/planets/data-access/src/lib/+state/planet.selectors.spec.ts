import {
  initialState,
  PLANET_FEATURE_KEY,
  PlanetState,
} from './planet.reducer';
import { planetQuery } from './planet.selectors';

describe('Planet Selectors', () => {
  let storeState: { [PLANET_FEATURE_KEY]: PlanetState };

  beforeEach(() => {
    storeState = {
      [PLANET_FEATURE_KEY]: initialState,
    };
  });

  test('getPlanet() returns planet value', () => {
    const result = planetQuery.getPlanet(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planet);
  });

  test('getPlanetLoading() returns planetLoading value', () => {
    const result = planetQuery.getPlanetLoading(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetLoading);
  });

  test('getPlanetLoadError() returns planetLoadError value', () => {
    const result = planetQuery.getPlanetLoadError(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetLoadError);
  });

  test('getPlanetCollection() returns planetCollection value', () => {
    const result = planetQuery.getPlanetCollection(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetCollection);
  });

  test('getPlanetCollectionLoading() returns planetCollectionLoading value', () => {
    const result = planetQuery.getPlanetCollectionLoading(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetCollectionLoading);
  });

  test('getPlanetCollectionLoadError() returns planetCollectionLoadError value', () => {
    const result = planetQuery.getPlanetCollectionLoadError(storeState);

    expect(result).toBe(
      storeState[PLANET_FEATURE_KEY].planetCollectionLoadError
    );
  });

  test('getPlanetCreating() returns planetCreating value', () => {
    const result = planetQuery.getPlanetCreating(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetCreating);
  });

  test('getPlanetCreateError() returns planetCreateError value', () => {
    const result = planetQuery.getPlanetCreateError(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetCreateError);
  });

  test('getPlanetUpdating() returns planetUpdating value', () => {
    const result = planetQuery.getPlanetUpdating(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetUpdating);
  });

  test('getPlanetUpdateError() returns planetUpdateError value', () => {
    const result = planetQuery.getPlanetUpdateError(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetUpdateError);
  });

  test('getPlanetRemoving() returns planetRemoving value', () => {
    const result = planetQuery.getPlanetRemoving(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetRemoving);
  });

  test('getPlanetRemoveError() returns planetRemoveError value', () => {
    const result = planetQuery.getPlanetRemoveError(storeState);

    expect(result).toBe(storeState[PLANET_FEATURE_KEY].planetRemoveError);
  });
});
