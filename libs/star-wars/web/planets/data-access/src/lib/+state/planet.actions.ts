import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { GetPlanetRequestPayload } from '../resources/request-payloads/get-planet.request-payload';
import { Planet, PlanetResponse } from '@star-wars/star-wars/web/planets/domain';

export namespace fromPlanetActions {
  export enum Types {
    GetPlanet = '[Planet] Get Planet',
    GetPlanetFail = '[Planet] Get Planet Fail',
    GetPlanetSuccess = '[Planet] Get Planet Success',
    GetPlanetCollection = '[Planet] Get Planet Collection',
    GetPlanetCollectionFail = '[Planet] Get Planet Collection Fail',
    GetPlanetCollectionSuccess = '[Planet] Get Planet Collection Success',
  }

  export class GetPlanet implements Action {
    readonly type = Types.GetPlanet;

    constructor(public payload: GetPlanetRequestPayload) {}
  }

  export class GetPlanetFail implements Action {
    readonly type = Types.GetPlanetFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetPlanetSuccess implements Action {
    readonly type = Types.GetPlanetSuccess;

    constructor(public payload: Planet) {}
  }

  export class GetPlanetCollection implements Action {
    readonly type = Types.GetPlanetCollection;

    constructor(public payload: number) {}
  }

  export class GetPlanetCollectionFail implements Action {
    readonly type = Types.GetPlanetCollectionFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetPlanetCollectionSuccess implements Action {
    readonly type = Types.GetPlanetCollectionSuccess;

    constructor(public payload: PlanetResponse) {}
  }

  export type CollectiveType =
    | GetPlanet
    | GetPlanetFail
    | GetPlanetSuccess
    | GetPlanetCollection
    | GetPlanetCollectionFail
    | GetPlanetCollectionSuccess;
}
