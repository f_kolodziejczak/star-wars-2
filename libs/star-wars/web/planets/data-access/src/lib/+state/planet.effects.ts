import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fromPlanetActions } from './planet.actions';
import { PlanetDataService } from '../services/planet-data.service';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class PlanetEffects {

  getPlanet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPlanetActions.Types.GetPlanet),
      mergeMap((action: fromPlanetActions.GetPlanet) =>
        this.planetDataService.getPlanet(action.payload),
      ),
      map((data) => new fromPlanetActions.GetPlanetSuccess(data)),
      catchError((error) => of(new fromPlanetActions.GetPlanetFail(error))),
    )
  )

  getPlanetCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPlanetActions.Types.GetPlanetCollection),
      mergeMap((action: fromPlanetActions.GetPlanetCollection) =>
        this.planetDataService.getPlanetCollection(action.payload),
      ),
      map((data) => new fromPlanetActions.GetPlanetCollectionSuccess(data)),
      catchError((error) => of(new fromPlanetActions.GetPlanetCollectionFail(error))),
    )
  );

  constructor(
    private planetDataService: PlanetDataService,
    private actions$: Actions,
  ) {}
}
