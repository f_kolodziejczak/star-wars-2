import { fromPlanetActions } from './planet.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { Planet, PlanetResponse } from '@star-wars/star-wars/web/planets/domain';

export const PLANET_FEATURE_KEY = 'planet';

export interface PlanetState {
  planet: Planet | null;
  planetLoading: boolean;
  planetLoadError: HttpErrorResponse | null;
  planetCollection: PlanetResponse | null;
  planetCollectionLoading: boolean;
  planetCollectionLoadError: HttpErrorResponse | null;
}

export interface PlanetPartialState {
  readonly [PLANET_FEATURE_KEY]: PlanetState;
}

export const initialState: PlanetState = {
  planet: null,
  planetLoading: false,
  planetLoadError: null,
  planetCollection: null,
  planetCollectionLoading: false,
  planetCollectionLoadError: null,
};

export function planetReducer(
  state: PlanetState = initialState,
  action: fromPlanetActions.CollectiveType
): PlanetState {
  switch (action.type) {
    case fromPlanetActions.Types.GetPlanet: {
      state = {
        ...state,
        planet: null,
        planetLoading: true,
        planetLoadError: null,
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetFail: {
      state = {
        ...state,
        planet: null,
        planetLoading: false,
        planetLoadError: action.payload,
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetSuccess: {
      state = {
        ...state,
        planet: action.payload,
        planetLoading: false,
        planetLoadError: null,
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetCollection: {
      state = {
        ...state,
        planetCollection: null,
        planetCollectionLoading: true,
        planetCollectionLoadError: null,
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetCollectionFail: {
      state = {
        ...state,
        planetCollection: null,
        planetCollectionLoading: false,
        planetCollectionLoadError: action.payload,
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetCollectionSuccess: {
      state = {
        ...state,
        planetCollection: action.payload,
        planetCollectionLoading: false,
        planetCollectionLoadError: null,
      };
      break;
    }
  }

  return state;
}
