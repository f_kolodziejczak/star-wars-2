import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GetPlanetRequestPayload } from '../resources/request-payloads/get-planet.request-payload';
import { map } from 'rxjs/operators';
import { Planet, PlanetResponse } from '@star-wars/star-wars/web/planets/domain';

@Injectable()
export class PlanetDataService {
  readonly endpoints = {
    getPlanet: 'https://swapi.dev/api/planets/',
    getPlanetCollection: 'https://swapi.dev/api/planets/',
  };
  constructor(private http: HttpClient) {}

  getPlanet(payload: GetPlanetRequestPayload): Observable<Planet> {
    return this.http.get<Planet>(this.endpoints.getPlanet + payload.id).pipe(
      map((planet) => {
        const mappedPlanet = {};
        Object.keys(planet).forEach((key) => {
          mappedPlanet[PlanetDataService.camelCaseMapper(key)] = planet[key];
        });
        return mappedPlanet as Planet;
      })
    );
  }

  getPlanetCollection(pageNumber: number): Observable<PlanetResponse> {
    return this.http
      .get<PlanetResponse>(this.endpoints.getPlanetCollection + `?page=${pageNumber}` )
      .pipe(
        map((response) => ({
          ...response,
          results: response.results.map((planet) => {
            const mappedPlanet = {};
            Object.keys(planet).forEach((key) => {
              mappedPlanet[PlanetDataService.camelCaseMapper(key)] =
                planet[key];
            });
            return mappedPlanet as Planet;
          }),
        }))
      );
  }

  private static camelCaseMapper(variableKey: string): string {
    return variableKey.replace(/([-_][a-z])/gi, (letter) =>
      letter.toUpperCase().replace('_', '')
    );
  }
}
