import { TestBed } from '@angular/core/testing';
import { PlanetDataService } from './planet-data.service';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';

describe('PlanetDataService', () => {
  let service: PlanetDataService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });

    service = TestBed.inject(PlanetDataService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
    localStorage.clear();
  });

  test('is created', () => {
    const service: PlanetDataService = TestBed.inject(PlanetDataService);
    expect(service).toBeTruthy();
  });

  describe('#getPlanet', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.getPlanet({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.getPlanet);
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.getPlanet({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.getPlanet);
      expect(req.request.method).toBe('GET');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#getPlanetCollection', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.getPlanetCollection({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.getPlanetCollection);
      expect(req.request.method).toBe('GET');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.getPlanetCollection({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.getPlanetCollection);
      expect(req.request.method).toBe('GET');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#createPlanet', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.createPlanet({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.createPlanet);
      expect(req.request.method).toBe('POST');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.createPlanet({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.createPlanet);
      expect(req.request.method).toBe('POST');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#updatePlanet', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.updatePlanet({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.updatePlanet);
      expect(req.request.method).toBe('PUT');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.updatePlanet({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.updatePlanet);
      expect(req.request.method).toBe('PUT');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });

  describe('#removePlanet', () => {
    test('returns an observable of response data on success', () => {
      const response = {} as any;

      service.removePlanet({} as any).subscribe((res) => {
        expect(res).toBe(response);
      });

      const req = httpMock.expectOne(service.endpoints.removePlanet);
      expect(req.request.method).toBe('DELETE');
      req.flush(response);
    });

    test('throws an error including response data on fail', () => {
      const response = {};

      service.removePlanet({} as any).subscribe(
        () => {
          fail('expecting error');
        },
        (err) => {
          expect(err.error).toBe(response);
        }
      );

      const req = httpMock.expectOne(service.endpoints.removePlanet);
      expect(req.request.method).toBe('DELETE');
      req.flush(response, {
        status: 400,
        statusText: 'Bad request',
      });
    });
  });
});
