import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  PLANET_FEATURE_KEY,
  planetReducer,
} from './+state/planet.reducer';
import { PlanetEffects } from './+state/planet.effects';
import { PlanetFacade } from './+state/planet.facade';
import { PlanetDataService } from './services/planet-data.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature(PLANET_FEATURE_KEY, planetReducer),
    EffectsModule.forRoot([PlanetEffects]),
  ],
  providers: [
    PlanetFacade,
    PlanetDataService,
  ],
})
export class StarWarsWebPlanetsDataAccessModule {}
