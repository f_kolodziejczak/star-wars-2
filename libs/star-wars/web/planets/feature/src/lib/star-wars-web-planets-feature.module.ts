import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetsComponent } from './planets/planets.component';
import { StarWarsWebSharedUiTableModule } from '@star-wars/star-wars/web/shared/ui-table';
import { MatDialogModule } from '@angular/material/dialog';
import { StarWarsWebPlanetsFeatureRoutingModule } from './star-wars-web-planets-feature-routing.module';
import { PlanetFacade } from '@star-wars/star-wars/web/planets/data-access';

@NgModule({
  imports: [
    CommonModule,
    StarWarsWebPlanetsFeatureRoutingModule,
    StarWarsWebSharedUiTableModule,
    MatDialogModule,
  ],
  declarations: [
    PlanetsComponent,
  ],
  providers: [
    PlanetFacade,
  ]
})
export class StarWarsWebPlanetsFeatureModule {}
