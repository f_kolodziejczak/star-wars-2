import { Component, OnDestroy, OnInit } from '@angular/core';
import { Column } from '@star-wars/star-wars/web/domain';
import { PlanetFacade } from '@star-wars/star-wars/web/planets/data-access';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DetailsDialogComponent } from '@star-wars/star-wars/web/shared/ui-details-dialog';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Planet, PlanetResponse } from '@star-wars/star-wars/web/planets/domain';

@Component({
  selector: 'star-wars-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.scss'],
})
export class PlanetsComponent implements OnInit, OnDestroy {
  planets$ = this.planetFacade.planetCollection$;
  planetsLoading$ = this.planetFacade.planetCollectionLoading$;
  planetData: PlanetResponse;
  columns: Column[] = [
    { columnId: 'name', displayedName: 'Name' },
    { columnId: 'population', displayedName: 'Population' },
    { columnId: 'climate', displayedName: 'Climate' },
    { columnId: 'terrain', displayedName: 'Terrain' },
    { columnId: 'gravity', displayedName: 'Gravity' },
    { columnId: 'rotationPeriod', displayedName: 'Rotation Period' },
    { columnId: 'orbitalPeriod', displayedName: 'Orbital Period' },
    { columnId: 'diameter', displayedName: 'Diameter' },
    { columnId: 'surfaceWater', displayedName: 'Surface Water' },
  ];
  private componentDestroyed$ = new Subject();
  constructor(private planetFacade: PlanetFacade, private dialog: MatDialog) {
    this.planetFacade.getPlanetCollection(1);
  }

  ngOnInit(): void {
    this.getInitialData();
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  openDetailsDialog(planet: Planet): void {
    this.planetFacade.getPlanet({ id: planet.url.split('/').slice(-2)[0] + '/' });
    this.dialog.open(DetailsDialogComponent, {
      width: '500px',
      maxHeight: '600px',
      data: {title: planet.name, data$: this.planetFacade.planet$},
    });
  }

  loadPaginatedData(page: PageEvent): void {
    if (
      (page.pageIndex > page.previousPageIndex && this.planetData.next) ||
      (page.pageIndex < page.previousPageIndex && this.planetData.previous)
    ) {
      this.planetFacade.getPlanetCollection(page.pageIndex + 1);
    }
  }

  private getInitialData(): void {
    this.planets$
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((data) => {
        if (data) {
          this.planetData = {
            results: data.results,
            count: data.count,
            next: data.next,
            previous: data.previous,
          };
        }
      });
  }
}
