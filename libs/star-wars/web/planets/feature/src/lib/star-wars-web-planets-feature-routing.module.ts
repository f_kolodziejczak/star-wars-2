import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { PlanetsComponent } from './planets/planets.component';

const routes: Route[] = [
  {
    path: '',
    component: PlanetsComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StarWarsWebPlanetsFeatureRoutingModule { }
