export interface Planet {
  id: string;
  url: string;
  climate: string;
  diameter: string;
  rotationPeriod: string
  surfaceWater: string
  terrain: string;
  gravity: string;
  name: string;
  orbitalPeriod: string;
  population: string;
}
