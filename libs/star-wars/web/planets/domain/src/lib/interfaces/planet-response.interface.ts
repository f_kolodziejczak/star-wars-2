import { TableData } from '@star-wars/star-wars/web/domain';
import { Planet } from './planet.interface';

export interface PlanetResponse extends TableData {
  results: Planet[];
}
