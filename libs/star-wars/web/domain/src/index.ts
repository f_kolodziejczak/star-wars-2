export * from './lib/interfaces/menu-item.interface';
export * from './lib/interfaces/column.interface';
export * from './lib/interfaces/details-dialog-data.interface';
export * from './lib/interfaces/table-data.interface';
