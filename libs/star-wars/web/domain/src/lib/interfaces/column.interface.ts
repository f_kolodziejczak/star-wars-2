export interface Column {
  columnId: string;
  displayedName: string;
}
