import { Observable } from 'rxjs';
import { Planet } from '@star-wars/star-wars/web/planets/domain';
import { Person } from '@star-wars/star-wars/web/people/domain';
import { Spaceship } from '@star-wars/star-wars/web/spaceships/domain';

export interface DetailsDialogData {
  title: string;
  data$: Observable<Planet | Person | Spaceship>;
}
