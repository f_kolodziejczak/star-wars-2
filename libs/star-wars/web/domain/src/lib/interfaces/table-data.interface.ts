export interface TableData {
  count: number;
  next: string;
  previous: string;
}
