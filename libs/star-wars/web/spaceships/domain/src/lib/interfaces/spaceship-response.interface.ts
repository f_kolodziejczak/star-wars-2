import { TableData } from '@star-wars/star-wars/web/domain';
import { Spaceship } from './spaceship.interface';

export interface SpaceshipResponse extends TableData{
  results: Spaceship[];
}
