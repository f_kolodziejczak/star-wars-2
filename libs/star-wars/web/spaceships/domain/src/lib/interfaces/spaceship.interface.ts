export interface Spaceship {
  name: string;
  model: string;
  starshipClass: string;
  manufacturer: string;
  costInCredits: string;
  length: string;
  crew: string;
  passengers: string;
  hyperdriveRating: string;
  cargoCapacity: string;
  url: string;
}
