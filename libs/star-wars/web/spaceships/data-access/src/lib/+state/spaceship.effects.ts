import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { fromSpaceshipActions } from './spaceship.actions';
import { SpaceshipDataService } from '../services/spaceship-data.service';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class SpaceshipEffects {
  getSpaceship$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSpaceshipActions.Types.GetSpaceship),
      mergeMap((action: fromSpaceshipActions.GetSpaceship) =>
        this.spaceshipDataService.getSpaceship(action.payload)
      ),
      map((data) => new fromSpaceshipActions.GetSpaceshipSuccess(data)),
      catchError((error) =>
        of(new fromSpaceshipActions.GetSpaceshipFail(error))
      )
    )
  );

  getSpaceshipCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSpaceshipActions.Types.GetSpaceshipCollection),
      mergeMap((action: fromSpaceshipActions.GetSpaceshipCollection) =>
        this.spaceshipDataService.getSpaceshipCollection(action.payload.page)
      ),
      map(
        (data) => new fromSpaceshipActions.GetSpaceshipCollectionSuccess(data)
      ),
      catchError((error) =>
        of(new fromSpaceshipActions.GetSpaceshipCollectionFail(error))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private spaceshipDataService: SpaceshipDataService
  ) {}
}
