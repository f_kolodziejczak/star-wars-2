import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SPACESHIP_FEATURE_KEY, SpaceshipState } from './spaceship.reducer';

// Lookup the 'Spaceship' feature state managed by NgRx
const getSpaceshipState = createFeatureSelector<SpaceshipState>(
  SPACESHIP_FEATURE_KEY
);

const getSpaceship = createSelector(
  getSpaceshipState,
  (state) => state.spaceship
);

const getSpaceshipLoading = createSelector(
  getSpaceshipState,
  (state) => state.spaceshipLoading
);

const getSpaceshipLoadError = createSelector(
  getSpaceshipState,
  (state) => state.spaceshipLoadError
);

const getSpaceshipCollection = createSelector(
  getSpaceshipState,
  (state) => state.spaceshipCollection
);

const getSpaceshipCollectionLoading = createSelector(
  getSpaceshipState,
  (state) => state.spaceshipCollectionLoading
);

const getSpaceshipCollectionLoadError = createSelector(
  getSpaceshipState,
  (state) => state.spaceshipCollectionLoadError
);

export const spaceshipQuery = {
  getSpaceship,
  getSpaceshipLoading,
  getSpaceshipLoadError,
  getSpaceshipCollection,
  getSpaceshipCollectionLoading,
  getSpaceshipCollectionLoadError,
};
