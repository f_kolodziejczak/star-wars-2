import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { SpaceshipPartialState } from './spaceship.reducer';
import { spaceshipQuery } from './spaceship.selectors';
import { fromSpaceshipActions } from './spaceship.actions';
import { GetSpaceshipRequestPayload } from '../resources/request-payloads/get-spaceship.request-payload';
import { GetSpaceshipCollectionRequestPayload } from '../resources/request-payloads/get-spaceship-collection.request-payload';

@Injectable()
export class SpaceshipFacade {
  spaceship$ = this.store.pipe(select(spaceshipQuery.getSpaceship));
  spaceshipLoading$ = this.store.pipe(
    select(spaceshipQuery.getSpaceshipLoading)
  );
  spaceshipLoadError$ = this.store.pipe(
    select(spaceshipQuery.getSpaceshipLoadError)
  );
  spaceshipCollection$ = this.store.pipe(
    select(spaceshipQuery.getSpaceshipCollection)
  );
  spaceshipCollectionLoading$ = this.store.pipe(
    select(spaceshipQuery.getSpaceshipCollectionLoading)
  );
  spaceshipCollectionLoadError$ = this.store.pipe(
    select(spaceshipQuery.getSpaceshipCollectionLoadError)
  );
  constructor(private store: Store<SpaceshipPartialState>) {}

  getSpaceship(data: GetSpaceshipRequestPayload): void {
    this.store.dispatch(new fromSpaceshipActions.GetSpaceship(data));
  }

  getSpaceshipCollection(data: GetSpaceshipCollectionRequestPayload): void {
    this.store.dispatch(new fromSpaceshipActions.GetSpaceshipCollection(data));
  }
}
