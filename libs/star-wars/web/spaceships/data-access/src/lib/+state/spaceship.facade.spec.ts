import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { NxModule } from '@nrwl/angular';
import { Observable } from 'rxjs';
import { SpaceshipFacade } from './spaceship.facade';
import { fromSpaceshipActions } from './spaceship.actions';

describe('SpaceshipFacade', () => {
  let actions: Observable<any>;
  let facade: SpaceshipFacade;
  let store: MockStore;

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [],
        providers: [
          SpaceshipFacade,
          provideMockStore(),
          provideMockActions(() => actions),
        ],
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [NxModule.forRoot(), CustomFeatureModule],
      })
      class RootModule {}

      TestBed.configureTestingModule({ imports: [RootModule] });
      facade = TestBed.inject(SpaceshipFacade);
      store = TestBed.inject(MockStore);
      jest.spyOn(store, 'dispatch');
    });

    describe('#getSpaceship', () => {
      test('should dispatch fromSpaceshipActions.GetSpaceship action', () => {
        const payload = {} as any;
        const action = new fromSpaceshipActions.GetSpaceship(payload);

        facade.getSpaceship(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });

    describe('#getSpaceshipCollection', () => {
      test('should dispatch fromSpaceshipActions.GetSpaceshipCollection action', () => {
        const payload = {} as any;
        const action = new fromSpaceshipActions.GetSpaceshipCollection(payload);

        facade.getSpaceshipCollection(payload);
        expect(store.dispatch).toHaveBeenCalledWith(action);
      });
    });
  });
});
