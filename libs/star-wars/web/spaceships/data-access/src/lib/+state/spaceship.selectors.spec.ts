import {
  initialState,
  SPACESHIP_FEATURE_KEY,
  SpaceshipState,
} from './spaceship.reducer';
import { spaceshipQuery } from './spaceship.selectors';

describe('Spaceship Selectors', () => {
  let storeState: { [SPACESHIP_FEATURE_KEY]: SpaceshipState };

  beforeEach(() => {
    storeState = {
      [SPACESHIP_FEATURE_KEY]: initialState,
    };
  });

  test('getSpaceship() returns spaceship value', () => {
    const result = spaceshipQuery.getSpaceship(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceship);
  });

  test('getSpaceshipLoading() returns spaceshipLoading value', () => {
    const result = spaceshipQuery.getSpaceshipLoading(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipLoading);
  });

  test('getSpaceshipLoadError() returns spaceshipLoadError value', () => {
    const result = spaceshipQuery.getSpaceshipLoadError(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipLoadError);
  });

  test('getSpaceshipCollection() returns spaceshipCollection value', () => {
    const result = spaceshipQuery.getSpaceshipCollection(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipCollection);
  });

  test('getSpaceshipCollectionLoading() returns spaceshipCollectionLoading value', () => {
    const result = spaceshipQuery.getSpaceshipCollectionLoading(storeState);

    expect(result).toBe(
      storeState[SPACESHIP_FEATURE_KEY].spaceshipCollectionLoading
    );
  });

  test('getSpaceshipCollectionLoadError() returns spaceshipCollectionLoadError value', () => {
    const result = spaceshipQuery.getSpaceshipCollectionLoadError(storeState);

    expect(result).toBe(
      storeState[SPACESHIP_FEATURE_KEY].spaceshipCollectionLoadError
    );
  });

  test('getSpaceshipCreating() returns spaceshipCreating value', () => {
    const result = spaceshipQuery.getSpaceshipCreating(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipCreating);
  });

  test('getSpaceshipCreateError() returns spaceshipCreateError value', () => {
    const result = spaceshipQuery.getSpaceshipCreateError(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipCreateError);
  });

  test('getSpaceshipUpdating() returns spaceshipUpdating value', () => {
    const result = spaceshipQuery.getSpaceshipUpdating(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipUpdating);
  });

  test('getSpaceshipUpdateError() returns spaceshipUpdateError value', () => {
    const result = spaceshipQuery.getSpaceshipUpdateError(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipUpdateError);
  });

  test('getSpaceshipRemoving() returns spaceshipRemoving value', () => {
    const result = spaceshipQuery.getSpaceshipRemoving(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipRemoving);
  });

  test('getSpaceshipRemoveError() returns spaceshipRemoveError value', () => {
    const result = spaceshipQuery.getSpaceshipRemoveError(storeState);

    expect(result).toBe(storeState[SPACESHIP_FEATURE_KEY].spaceshipRemoveError);
  });
});
