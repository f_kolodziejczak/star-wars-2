import { fromSpaceshipActions } from './spaceship.actions';
import {
  SpaceshipState,
  initialState,
  spaceshipReducer,
} from './spaceship.reducer';
import { statesEqual } from '@valueadd/testing';

describe('Spaceship Reducer', () => {
  let state: SpaceshipState;

  beforeEach(() => {
    state = { ...initialState };
  });

  describe('unknown action', () => {
    test('returns the initial state', () => {
      const action = {} as any;
      const result = spaceshipReducer(state, action);

      expect(result).toBe(state);
    });
  });

  describe('GetSpaceship', () => {
    test('sets spaceship, spaceshipLoading, spaceshipLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceship(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceship).toEqual(null);
      expect(result.spaceshipLoading).toEqual(true);
      expect(result.spaceshipLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceship',
          'spaceshipLoading',
          'spaceshipLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetSpaceshipFail', () => {
    test('sets spaceship, spaceshipLoading, spaceshipLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipFail(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceship).toEqual(null);
      expect(result.spaceshipLoading).toEqual(false);
      expect(result.spaceshipLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'spaceship',
          'spaceshipLoading',
          'spaceshipLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetSpaceshipSuccess', () => {
    test('sets spaceship, spaceshipLoading, spaceshipLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipSuccess(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceship).toEqual(payload);
      expect(result.spaceshipLoading).toEqual(false);
      expect(result.spaceshipLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceship',
          'spaceshipLoading',
          'spaceshipLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetSpaceshipCollection', () => {
    test('sets spaceshipCollection, spaceshipCollectionLoading, spaceshipCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipCollection(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCollection).toEqual([]);
      expect(result.spaceshipCollectionLoading).toEqual(true);
      expect(result.spaceshipCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipCollection',
          'spaceshipCollectionLoading',
          'spaceshipCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetSpaceshipCollectionFail', () => {
    test('sets spaceshipCollection, spaceshipCollectionLoading, spaceshipCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipCollectionFail(
        payload
      );
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCollection).toEqual([]);
      expect(result.spaceshipCollectionLoading).toEqual(false);
      expect(result.spaceshipCollectionLoadError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'spaceshipCollection',
          'spaceshipCollectionLoading',
          'spaceshipCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('GetSpaceshipCollectionSuccess', () => {
    test('sets spaceshipCollection, spaceshipCollectionLoading, spaceshipCollectionLoadError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipCollectionSuccess(
        payload
      );
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCollection).toEqual(payload);
      expect(result.spaceshipCollectionLoading).toEqual(false);
      expect(result.spaceshipCollectionLoadError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipCollection',
          'spaceshipCollectionLoading',
          'spaceshipCollectionLoadError',
        ])
      ).toBeTruthy();
    });
  });

  describe('CreateSpaceship', () => {
    test('sets spaceshipCreating, spaceshipCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.CreateSpaceship(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCreating).toEqual(true);
      expect(result.spaceshipCreateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipCreating',
          'spaceshipCreateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('CreateSpaceshipFail', () => {
    test('sets spaceshipCreating, spaceshipCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.CreateSpaceshipFail(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCreating).toEqual(false);
      expect(result.spaceshipCreateError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'spaceshipCreating',
          'spaceshipCreateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('CreateSpaceshipSuccess', () => {
    test('sets spaceshipCollection, spaceshipCreating, spaceshipCreateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.CreateSpaceshipSuccess(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCollection.length).toEqual(1);
      expect(result.spaceshipCreating).toEqual(false);
      expect(result.spaceshipCreateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipCollection',
          'spaceshipCreating',
          'spaceshipCreateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('UpdateSpaceship', () => {
    test('sets spaceshipUpdating, spaceshipUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.UpdateSpaceship(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipUpdating).toEqual(true);
      expect(result.spaceshipUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipUpdating',
          'spaceshipUpdateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('UpdateSpaceshipFail', () => {
    test('sets spaceshipUpdating, spaceshipUpdateError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.UpdateSpaceshipFail(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipUpdating).toEqual(false);
      expect(result.spaceshipUpdateError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'spaceshipUpdating',
          'spaceshipUpdateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('UpdateSpaceshipSuccess', () => {
    test('sets spaceshipCollection, spaceshipUpdating, spaceshipUpdateError and does not modify other state properties', () => {
      state = {
        ...initialState,
        spaceshipCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromSpaceshipActions.UpdateSpaceshipSuccess(payload);
      const result = spaceshipReducer(state, action);

      expect((result as any).spaceshipCollection[0].name).toEqual('test2');
      expect((result as any).spaceshipUpdating).toEqual(false);
      expect((result as any).spaceshipUpdateError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipCollection',
          'spaceshipUpdating',
          'spaceshipUpdateError',
        ])
      ).toBeTruthy();
    });
  });

  describe('RemoveSpaceship', () => {
    test('sets spaceshipRemoving, spaceshipRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.RemoveSpaceship(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipRemoving).toEqual(true);
      expect(result.spaceshipRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipRemoving',
          'spaceshipRemoveError',
        ])
      ).toBeTruthy();
    });
  });

  describe('RemoveSpaceshipFail', () => {
    test('sets spaceshipRemoving, spaceshipRemoveError and does not modify other state properties', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.RemoveSpaceshipFail(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipRemoving).toEqual(false);
      expect(result.spaceshipRemoveError).toEqual(payload);
      expect(
        statesEqual(result, state, [
          'spaceshipRemoving',
          'spaceshipRemoveError',
        ])
      ).toBeTruthy();
    });
  });

  describe('RemoveSpaceshipSuccess', () => {
    test('sets spaceshipCollection, spaceshipRemoving, spaceshipRemoveError and does not modify other state properties', () => {
      state = {
        ...initialState,
        spaceshipCollection: [{ id: '1', name: 'test' } as any],
      };
      const payload = { id: '1', name: 'test2' } as any;
      const action = new fromSpaceshipActions.RemoveSpaceshipSuccess(payload);
      const result = spaceshipReducer(state, action);

      expect(result.spaceshipCollection.length).toEqual(0);
      expect(result.spaceshipRemoving).toEqual(false);
      expect(result.spaceshipRemoveError).toEqual(null);
      expect(
        statesEqual(result, state, [
          'spaceshipCollection',
          'spaceshipRemoving',
          'spaceshipRemoveError',
        ])
      ).toBeTruthy();
    });
  });
});
