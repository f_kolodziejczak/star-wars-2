import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { GetSpaceshipRequestPayload } from '../resources/request-payloads/get-spaceship.request-payload';
import { GetSpaceshipCollectionRequestPayload } from '../resources/request-payloads/get-spaceship-collection.request-payload';
import { Spaceship, SpaceshipResponse } from '@star-wars/star-wars/web/spaceships/domain';

export namespace fromSpaceshipActions {
  export enum Types {
    GetSpaceship = '[Spaceships] Get Spaceship',
    GetSpaceshipFail = '[Spaceships] Get Spaceship Fail',
    GetSpaceshipSuccess = '[Spaceships] Get Spaceship Success',
    GetSpaceshipCollection = '[Spaceships] Get Spaceship Collection',
    GetSpaceshipCollectionFail = '[Spaceships] Get Spaceship Collection Fail',
    GetSpaceshipCollectionSuccess = '[Spaceships] Get Spaceship Collection Success',
  }

  export class GetSpaceship implements Action {
    readonly type = Types.GetSpaceship;

    constructor(public payload: GetSpaceshipRequestPayload) {}
  }

  export class GetSpaceshipFail implements Action {
    readonly type = Types.GetSpaceshipFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetSpaceshipSuccess implements Action {
    readonly type = Types.GetSpaceshipSuccess;

    constructor(public payload: Spaceship) {}
  }

  export class GetSpaceshipCollection implements Action {
    readonly type = Types.GetSpaceshipCollection;

    constructor(public payload: GetSpaceshipCollectionRequestPayload) {}
  }

  export class GetSpaceshipCollectionFail implements Action {
    readonly type = Types.GetSpaceshipCollectionFail;

    constructor(public payload: HttpErrorResponse) {}
  }

  export class GetSpaceshipCollectionSuccess implements Action {
    readonly type = Types.GetSpaceshipCollectionSuccess;

    constructor(public payload: SpaceshipResponse) {}
  }

  export type CollectiveType =
    | GetSpaceship
    | GetSpaceshipFail
    | GetSpaceshipSuccess
    | GetSpaceshipCollection
    | GetSpaceshipCollectionFail
    | GetSpaceshipCollectionSuccess;
}
