import { fromSpaceshipActions } from './spaceship.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { Spaceship, SpaceshipResponse } from '@star-wars/star-wars/web/spaceships/domain';

export const SPACESHIP_FEATURE_KEY = 'spaceship';

export interface SpaceshipState {
  spaceship: Spaceship | null;
  spaceshipLoading: boolean;
  spaceshipLoadError: HttpErrorResponse | null;
  spaceshipCollection: SpaceshipResponse | null;
  spaceshipCollectionLoading: boolean;
  spaceshipCollectionLoadError: HttpErrorResponse | null;
}

export interface SpaceshipPartialState {
  readonly [SPACESHIP_FEATURE_KEY]: SpaceshipState;
}

export const initialState: SpaceshipState = {
  spaceship: null,
  spaceshipLoading: false,
  spaceshipLoadError: null,
  spaceshipCollection: null,
  spaceshipCollectionLoading: false,
  spaceshipCollectionLoadError: null,
};

export function spaceshipReducer(
  state: SpaceshipState = initialState,
  action: fromSpaceshipActions.CollectiveType
): SpaceshipState {
  switch (action.type) {
    case fromSpaceshipActions.Types.GetSpaceship: {
      state = {
        ...state,
        spaceship: null,
        spaceshipLoading: true,
        spaceshipLoadError: null,
      };
      break;
    }

    case fromSpaceshipActions.Types.GetSpaceshipFail: {
      state = {
        ...state,
        spaceship: null,
        spaceshipLoading: false,
        spaceshipLoadError: action.payload,
      };
      break;
    }

    case fromSpaceshipActions.Types.GetSpaceshipSuccess: {
      state = {
        ...state,
        spaceship: action.payload,
        spaceshipLoading: false,
        spaceshipLoadError: null,
      };
      break;
    }

    case fromSpaceshipActions.Types.GetSpaceshipCollection: {
      state = {
        ...state,
        spaceshipCollection: null,
        spaceshipCollectionLoading: true,
        spaceshipCollectionLoadError: null,
      };
      break;
    }

    case fromSpaceshipActions.Types.GetSpaceshipCollectionFail: {
      state = {
        ...state,
        spaceshipCollection: null,
        spaceshipCollectionLoading: false,
        spaceshipCollectionLoadError: action.payload,
      };
      break;
    }

    case fromSpaceshipActions.Types.GetSpaceshipCollectionSuccess: {
      state = {
        ...state,
        spaceshipCollection: action.payload,
        spaceshipCollectionLoading: false,
        spaceshipCollectionLoadError: null,
      };
      break;
    }
  }

  return state;
}
