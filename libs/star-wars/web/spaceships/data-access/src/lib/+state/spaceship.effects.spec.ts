import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { NxModule, DataPersistence } from '@nrwl/angular';
import { cold, hot } from 'jest-marbles';
import { SpaceshipEffects } from './spaceship.effects';
import { fromSpaceshipActions } from './spaceship.actions';
import { SpaceshipDataService } from '../services/spaceship-data.service';
import { createSpyObj } from 'jest-createspyobj';

describe('SpaceshipEffects', () => {
  let spaceshipDataService: jest.Mocked<SpaceshipDataService>;
  let actions: Observable<any>;
  let effects: SpaceshipEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        SpaceshipEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore({ initialState: {} }),
        {
          provide: SpaceshipDataService,
          useValue: createSpyObj(SpaceshipDataService),
        },
      ],
    });

    effects = TestBed.inject(SpaceshipEffects);
    spaceshipDataService = TestBed.inject(
      SpaceshipDataService
    ) as jest.Mocked<SpaceshipDataService>;
  });

  describe('getSpaceship$', () => {
    test('returns GetSpaceshipSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceship({} as any);
      const completion = new fromSpaceshipActions.GetSpaceshipSuccess(payload);

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      spaceshipDataService.getSpaceship.mockReturnValue(response);

      expect(effects.getSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.getSpaceship).toHaveBeenCalled();
      });
      expect(effects.getSpaceship$).toBeObservable(expected);
    });

    test('returns GetSpaceshipFail action on fail', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceship({} as any);
      const completion = new fromSpaceshipActions.GetSpaceshipFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      spaceshipDataService.getSpaceship.mockReturnValue(response);

      expect(effects.getSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.getSpaceship).toHaveBeenCalled();
      });
      expect(effects.getSpaceship$).toBeObservable(expected);
    });
  });

  describe('getSpaceshipCollection$', () => {
    test('returns GetSpaceshipCollectionSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipCollection({} as any);
      const completion = new fromSpaceshipActions.GetSpaceshipCollectionSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      spaceshipDataService.getSpaceshipCollection.mockReturnValue(response);

      expect(effects.getSpaceshipCollection$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.getSpaceshipCollection).toHaveBeenCalled();
      });
      expect(effects.getSpaceshipCollection$).toBeObservable(expected);
    });

    test('returns GetSpaceshipCollectionFail action on fail', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.GetSpaceshipCollection({} as any);
      const completion = new fromSpaceshipActions.GetSpaceshipCollectionFail(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      spaceshipDataService.getSpaceshipCollection.mockReturnValue(response);

      expect(effects.getSpaceshipCollection$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.getSpaceshipCollection).toHaveBeenCalled();
      });
      expect(effects.getSpaceshipCollection$).toBeObservable(expected);
    });
  });

  describe('createSpaceship$', () => {
    test('returns CreateSpaceshipSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.CreateSpaceship({} as any);
      const completion = new fromSpaceshipActions.CreateSpaceshipSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      spaceshipDataService.createSpaceship.mockReturnValue(response);

      expect(effects.createSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.createSpaceship).toHaveBeenCalled();
      });
      expect(effects.createSpaceship$).toBeObservable(expected);
    });

    test('returns CreateSpaceshipFail action on fail', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.CreateSpaceship({} as any);
      const completion = new fromSpaceshipActions.CreateSpaceshipFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      spaceshipDataService.createSpaceship.mockReturnValue(response);

      expect(effects.createSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.createSpaceship).toHaveBeenCalled();
      });
      expect(effects.createSpaceship$).toBeObservable(expected);
    });
  });

  describe('updateSpaceship$', () => {
    test('returns UpdateSpaceshipSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.UpdateSpaceship({} as any);
      const completion = new fromSpaceshipActions.UpdateSpaceshipSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      spaceshipDataService.updateSpaceship.mockReturnValue(response);

      expect(effects.updateSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.updateSpaceship).toHaveBeenCalled();
      });
      expect(effects.updateSpaceship$).toBeObservable(expected);
    });

    test('returns UpdateSpaceshipFail action on fail', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.UpdateSpaceship({} as any);
      const completion = new fromSpaceshipActions.UpdateSpaceshipFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      spaceshipDataService.updateSpaceship.mockReturnValue(response);

      expect(effects.updateSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.updateSpaceship).toHaveBeenCalled();
      });
      expect(effects.updateSpaceship$).toBeObservable(expected);
    });
  });

  describe('removeSpaceship$', () => {
    test('returns RemoveSpaceshipSuccess action on success', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.RemoveSpaceship({} as any);
      const completion = new fromSpaceshipActions.RemoveSpaceshipSuccess(
        payload
      );

      actions = hot('-a', { a: action });
      const response = cold('--b|', { b: payload });
      const expected = cold('---c', { c: completion });
      spaceshipDataService.removeSpaceship.mockReturnValue(response);

      expect(effects.removeSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.removeSpaceship).toHaveBeenCalled();
      });
      expect(effects.removeSpaceship$).toBeObservable(expected);
    });

    test('returns RemoveSpaceshipFail action on fail', () => {
      const payload = {} as any;
      const action = new fromSpaceshipActions.RemoveSpaceship({} as any);
      const completion = new fromSpaceshipActions.RemoveSpaceshipFail(payload);

      actions = hot('-a', { a: action });
      const response = cold('-#', {}, payload);
      const expected = cold('--c', { c: completion });
      spaceshipDataService.removeSpaceship.mockReturnValue(response);

      expect(effects.removeSpaceship$).toSatisfyOnFlush(() => {
        expect(spaceshipDataService.removeSpaceship).toHaveBeenCalled();
      });
      expect(effects.removeSpaceship$).toBeObservable(expected);
    });
  });
});
