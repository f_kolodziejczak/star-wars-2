import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  SPACESHIP_FEATURE_KEY,
  initialState as spaceshipInitialState,
  spaceshipReducer,
} from './+state/spaceship.reducer';
import { SpaceshipEffects } from './+state/spaceship.effects';
import { SpaceshipFacade } from './+state/spaceship.facade';
import { SpaceshipDataService } from './services/spaceship-data.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(SPACESHIP_FEATURE_KEY, spaceshipReducer, {
      initialState: spaceshipInitialState,
    }),
    EffectsModule.forFeature([SpaceshipEffects]),
  ],
  providers: [SpaceshipFacade, SpaceshipDataService],
})
export class StarWarsWebSpaceshipsDataAccessModule {}
