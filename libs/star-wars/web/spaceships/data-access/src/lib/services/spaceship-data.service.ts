import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { GetSpaceshipRequestPayload } from '../resources/request-payloads/get-spaceship.request-payload';
import { Spaceship, SpaceshipResponse } from '@star-wars/star-wars/web/spaceships/domain';

@Injectable()
export class SpaceshipDataService {
  readonly endpoints = {
    getSpaceship: 'https://swapi.dev/api/starships/',
    getSpaceshipCollection: 'https://swapi.dev/api/starships/',
  };
  constructor(private http: HttpClient) {}

  getSpaceship(payload: GetSpaceshipRequestPayload): Observable<Spaceship> {
    return this.http.get<Spaceship>(this.endpoints.getSpaceship + payload.id).pipe(
      map((spaceship) => {
        const mappedSpaceship = {};
        Object.keys(spaceship).forEach((key) => {
          mappedSpaceship[SpaceshipDataService.camelCaseMapper(key)] =
            spaceship[key];
        });
        return mappedSpaceship as Spaceship;
      })
    );
  }

  getSpaceshipCollection(pageNumber: number): Observable<SpaceshipResponse> {
    return this.http
      .get<SpaceshipResponse>(
        this.endpoints.getSpaceshipCollection + `?page=${pageNumber}`
      )
      .pipe(
        map((response) => ({
          ...response,
          results: response.results.map((spaceship) => {
            const mappedSpaceship = {};
            Object.keys(spaceship).forEach((key) => {
              mappedSpaceship[SpaceshipDataService.camelCaseMapper(key)] =
                spaceship[key];
            });
            return mappedSpaceship as Spaceship;
          }),
        }))
      );
  }

  private static camelCaseMapper(variableKey: string): string {
    return variableKey.replace(/([-_][a-z])/gi, (letter) =>
      letter.toUpperCase().replace('_', '')
    );
  }
}
