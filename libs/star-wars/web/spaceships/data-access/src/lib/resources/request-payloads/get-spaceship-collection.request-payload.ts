// tslint:disable-next-line:no-empty-interface
export interface GetSpaceshipCollectionRequestPayload {
  page: number;
}
