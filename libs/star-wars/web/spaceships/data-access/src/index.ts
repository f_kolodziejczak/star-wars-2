export * from './lib/+state/spaceship.facade';
export * from './lib/+state/spaceship.reducer';
export * from './lib/+state/spaceship.selectors';
export * from './lib/star-wars-web-spaceships-data-access.module';
