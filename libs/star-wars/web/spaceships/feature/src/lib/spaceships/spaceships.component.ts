import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Column } from '@star-wars/star-wars/web/domain';
import { MatDialog } from '@angular/material/dialog';
import { DetailsDialogComponent } from '@star-wars/star-wars/web/shared/ui-details-dialog';
import { Subject } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { takeUntil } from 'rxjs/operators';
import { SpaceshipFacade } from '@star-wars/star-wars/web/spaceships/data-access';
import {
  Spaceship,
  SpaceshipResponse,
} from '@star-wars/star-wars/web/spaceships/domain';

@Component({
  selector: 'star-wars-spaceships',
  templateUrl: './spaceships.component.html',
  styleUrls: ['./spaceships.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpaceshipsComponent implements OnInit, OnDestroy {
  spaceships$ = this.spaceshipFacade.spaceshipCollection$;
  spaceshipsLoading$ = this.spaceshipFacade.spaceshipCollectionLoading$;
  spaceshipData: SpaceshipResponse;
  columns: Column[] = [
    { columnId: 'name', displayedName: 'Name' },
    { columnId: 'model', displayedName: 'Model' },
    { columnId: 'starshipClass', displayedName: 'Starship Class' },
    { columnId: 'manufacturer', displayedName: 'Manufacturer' },
    { columnId: 'length', displayedName: 'Length' },
    { columnId: 'crew', displayedName: 'Crew' },
    { columnId: 'passengers', displayedName: 'Passengers' },
    { columnId: 'hyperdriveRating', displayedName: 'Hyperdrive rating' },
  ];
  private componentDestroyed$ = new Subject();
  constructor(
    private spaceshipFacade: SpaceshipFacade,
    private dialog: MatDialog
  ) {
    this.spaceshipFacade.getSpaceshipCollection({ page: 1 });
  }

  ngOnInit(): void {
    this.getInitialData();
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  openDetailsDialog(spaceship: Spaceship): void {
    this.spaceshipFacade.getSpaceship({
      id: spaceship.url.split('/').slice(-2)[0] + '/',
    });
    this.dialog.open(DetailsDialogComponent, {
      width: '500px',
      maxHeight: '800px',
      data: { title: spaceship.name, data$: this.spaceshipFacade.spaceship$ },
    });
  }

  loadPaginatedData(page: PageEvent): void {
    if (
      (page.pageIndex > page.previousPageIndex && this.spaceshipData.next) ||
      (page.pageIndex < page.previousPageIndex && this.spaceshipData.previous)
    ) {
      this.spaceshipFacade.getSpaceshipCollection({ page: page.pageIndex + 1 });
    }
  }

  private getInitialData(): void {
    this.spaceships$
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((data) => {
        if (data) {
          this.spaceshipData = {
            results: data.results,
            count: data.count,
            next: data.next,
            previous: data.previous,
          };
        }
      });
  }
}
