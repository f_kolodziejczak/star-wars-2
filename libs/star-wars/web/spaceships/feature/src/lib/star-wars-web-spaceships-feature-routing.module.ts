import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { SpaceshipsComponent } from './spaceships/spaceships.component';

const routes: Route[] = [
  {
    path: '',
    component: SpaceshipsComponent
  }
]


@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class StarWarsWebSpaceshipsFeatureRoutingModule { }
