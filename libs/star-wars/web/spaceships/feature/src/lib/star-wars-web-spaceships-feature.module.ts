import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaceshipsComponent } from './spaceships/spaceships.component';
import { StarWarsWebSpaceshipsFeatureRoutingModule } from './star-wars-web-spaceships-feature-routing.module';
import { StarWarsWebSharedUiTableModule } from '@star-wars/star-wars/web/shared/ui-table';
import { MatDialogModule } from '@angular/material/dialog';
import { SpaceshipFacade } from '@star-wars/star-wars/web/spaceships/data-access';

@NgModule({
  imports: [
    CommonModule,
    StarWarsWebSpaceshipsFeatureRoutingModule,
    StarWarsWebSharedUiTableModule,
    MatDialogModule,
  ],
  declarations: [
    SpaceshipsComponent
  ],
  providers: [
    SpaceshipFacade,
  ]
})
export class StarWarsWebSpaceshipsFeatureModule {}
