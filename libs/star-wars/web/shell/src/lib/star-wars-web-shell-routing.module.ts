import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'spaceships',
        pathMatch: 'full'
      },
      {
        path: 'spaceships',
        loadChildren: () => import('libs/star-wars/web/spaceships/feature/src/lib/star-wars-web-spaceships-feature.module').then(m => m.StarWarsWebSpaceshipsFeatureModule)
      },
      {
        path: 'people',
        loadChildren: () => import('libs/star-wars/web/people/feature/src/lib/star-wars-web-people-feature.module').then(m => m.StarWarsWebPeopleFeatureModule)
      },
      {
        path: 'planets',
        loadChildren: () => import('libs/star-wars/web/planets/feature/src/lib/star-wars-web-planets-feature.module').then(m => m.StarWarsWebPlanetsFeatureModule)
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class StarWarsWebShellRoutingModule { }
