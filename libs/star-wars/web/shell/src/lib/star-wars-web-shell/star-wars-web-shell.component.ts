import { Component } from '@angular/core';

@Component({
  selector: 'star-wars-web-shell',
  templateUrl: './star-wars-web-shell.component.html',
  styleUrls: ['./star-wars-web-shell.component.scss']
})
export class StarWarsWebShellComponent {}
