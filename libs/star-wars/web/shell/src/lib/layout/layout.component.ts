import { Component } from '@angular/core';
import { MenuItem } from '@star-wars/star-wars/web/domain';

@Component({
  selector: 'star-wars-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {

  menuItems: MenuItem[] = [
    { name: 'Planets', route: 'planets', icon: 'blur_circular' },
    { name: 'Spaceships', route: 'spaceships', icon: 'airplanemode_active' },
    { name: 'People', route: 'people', icon: 'people' }
  ];
}
