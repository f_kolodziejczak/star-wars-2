import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarWarsWebSharedUiGlobalHeaderModule } from '@star-wars/star-wars/web/shared/ui-global-header';
import { StarWarsWebShellRoutingModule } from './star-wars-web-shell-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { StarWarsWebShellComponent } from './star-wars-web-shell/star-wars-web-shell.component';

@NgModule({
  imports: [
    CommonModule,
    StarWarsWebSharedUiGlobalHeaderModule,
    StarWarsWebShellRoutingModule,
  ],
  declarations: [
    StarWarsWebShellComponent,
    LayoutComponent
  ],
  exports: [
    StarWarsWebShellComponent
  ]
})
export class StarWarsWebShellModule {}
