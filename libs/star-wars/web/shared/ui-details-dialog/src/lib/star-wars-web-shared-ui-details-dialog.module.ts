import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsDialogComponent } from './details-dialog/details-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { DisplayLabelPipe } from './details-dialog/display-label.pipe';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
  declarations: [DetailsDialogComponent, DisplayLabelPipe],
})
export class StarWarsWebSharedUiDetailsDialogModule {}
