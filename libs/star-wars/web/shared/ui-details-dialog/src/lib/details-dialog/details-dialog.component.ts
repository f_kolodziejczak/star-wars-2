import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DetailsDialogData } from '@star-wars/star-wars/web/domain';

@Component({
  selector: 'star-wars-details-dialog',
  templateUrl: './details-dialog.component.html',
  styleUrls: ['./details-dialog.component.scss'],
})
export class DetailsDialogComponent implements OnInit {
  detailsArray = [];
  dataLoading: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DetailsDialogData) {}

  ngOnInit(): void {
    this.dataLoading = true;
    this.data.data$.subscribe((response) => {
      if (response) {
        this.detailsArray = Object.keys(response).map((key) => [
          key,
          response[key],
        ]);
        this.dataLoading = false;
      }
    });
  }
}
