import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayLabel'
})
export class DisplayLabelPipe implements PipeTransform {

  transform(label: string): string {
    const newLabel = label[0].toUpperCase() + label.substr(1);
    return newLabel.replace(/[A-Z]/g, (letter) => ` ${letter}`);
  }

}
