import { Component, Input } from '@angular/core';
import { MenuItem } from '@star-wars/star-wars/web/domain';

@Component({
  selector: 'star-wars-global-header',
  templateUrl: './global-header.component.html',
  styleUrls: ['./global-header.component.scss']
})
export class GlobalHeaderComponent {
  @Input() menuItems: MenuItem[];
}
