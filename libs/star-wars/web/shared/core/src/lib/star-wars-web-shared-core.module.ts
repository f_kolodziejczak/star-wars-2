import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { NxModule } from '@nrwl/angular';

@NgModule({
  imports: [
    NxModule.forRoot(),
    StoreModule.forRoot(
      {},
      {}
    )
  ],
})
export class StarWarsWebSharedCoreModule {}
