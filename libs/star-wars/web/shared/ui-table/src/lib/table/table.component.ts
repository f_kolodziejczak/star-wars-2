import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { Column } from '@star-wars/star-wars/web/domain';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Planet } from '@star-wars/star-wars/web/planets/domain';
import { Person } from '@star-wars/star-wars/web/people/domain';
import { Spaceship } from '@star-wars/star-wars/web/spaceships/domain';

@Component({
  selector: 'star-wars-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Input() set data(data: (Planet | Person | Spaceship)[]) {
    this.appendActionColumn();
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
  }
  @Input() displayedColumns: Column[] = [];
  @Input() pageSize: number;
  @Input() paginatorLength: number;
  @Input() dataLoading: boolean;

  @Output() detailsDisplayed = new EventEmitter<Planet | Person | Spaceship>();
  @Output() changedPage = new EventEmitter<PageEvent>();

  dataSource: MatTableDataSource<Planet | Person | Spaceship>;
  columnsToDisplay: Column[];

  appendActionColumn(): void {
    this.columnsToDisplay = [...this.displayedColumns];
    this.columnsToDisplay.push({
      columnId: 'actions',
      displayedName: 'Actions',
    });
  }

  onPageChange  (page: PageEvent) {
    this.changedPage.emit(page);
  }
}
