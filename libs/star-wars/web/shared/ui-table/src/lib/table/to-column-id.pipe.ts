import { Pipe, PipeTransform } from '@angular/core';
import { Column } from '@star-wars/star-wars/web/domain';

@Pipe({
  name: 'toColumnId'
})
export class ToColumnIdPipe implements PipeTransform {

  transform(columns: Column[]): string[] {
    return columns.map(column => column.columnId);
  }

}
