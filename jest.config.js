module.exports = {
  projects: [
    '<rootDir>/apps/star-wars',
    '<rootDir>/libs/star-wars/web/shell',
    '<rootDir>/libs/star-wars/web/shared/core',
    '<rootDir>/libs/star-wars/web/shared/ui-global-header',
    '<rootDir>/libs/star-wars/web/spaceships/feature',
    '<rootDir>/libs/star-wars/web/domain',
    '<rootDir>/libs/star-wars/web/shared/ui-table',
    '<rootDir>/libs/star-wars/web/shared/ui-details-dialog',
    '<rootDir>/libs/star-wars/web/planets/data-access',
    '<rootDir>/libs/star-wars/web/spaceships/data-access',
    '<rootDir>/libs/star-wars/web/people/data-access',
    '<rootDir>/libs/star-wars/web/planets/domain',
    '<rootDir>/libs/star-wars/web/people/domain',
    '<rootDir>/libs/star-wars/web/spaceships/domain',
  ],
};
